// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class FIT2096_Lab05EditorTarget : TargetRules
{
	public FIT2096_Lab05EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "FIT2096_Lab05" } );
	}
}
