// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class FIT2096_Lab05Target : TargetRules
{
	public FIT2096_Lab05Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "FIT2096_Lab05" } );
	}
}
