// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MovingSphere.h"
#include "Components/TextRenderComponent.h"
#include "SpawnerActor.generated.h"

UCLASS()
class FIT2096_LAB05_API ASpawnerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnerActor();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float SpawnInterval;

	// How long, in seconds, the countdown will run
	UPROPERTY(EditAnywhere)
		int32 StartTime;

	// The reference to the object we are going to spawn
	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		TSubclassOf<AActor> SpawnObject;

	// Checkpoints for the spawned object
	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		TArray<FVector> Points;
	// Acceleration for the spawned object
	UPROPERTY(EditAnywhere, Category = "SpawnObject")
		float Acceleration;

	// Variables to handle the spawning
	bool StartSpawning = false;
	float SpawnCountdown;

	// Text countdown
	UTextRenderComponent* StartText;

	// Countdown functions
	void UpdateTimerDisplay();
	void AdvanceTimer();
	void CountdownHasFinished();

	// Timer handle
	FTimerHandle StartTimerHandle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
