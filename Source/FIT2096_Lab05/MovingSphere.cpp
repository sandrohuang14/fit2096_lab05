// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingSphere.h"

// Sets default values
AMovingSphere::AMovingSphere()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	SphereVisual->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(
		TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
	}
	TargetCheckpoint = 0;
	BaseAcceleration = 2;
	CurrentAcceleration = 0;
}

// Called when the game starts or when spawned
void AMovingSphere::BeginPlay()
{
	Super::BeginPlay();
	//Checkpoints.Add(FVector(0, 0, 0));
	//Checkpoints.Add(FVector(1500, 0, 0));
	//Checkpoints.Add(FVector(1500, 1500, 0));
	//Checkpoints.Add(FVector(0, 1500, 0));
}

// Called every frame
void AMovingSphere::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	RootComponent->SetWorldLocation(FMath::Lerp(this->GetActorLocation(),
		Checkpoints[TargetCheckpoint], DeltaTime * CurrentAcceleration));
	CurrentAcceleration += BaseAcceleration * DeltaTime;

	if (FVector::Dist(this->GetActorLocation(), Checkpoints[TargetCheckpoint]) < 2.0f)
	{
		TargetCheckpoint++;
		CurrentAcceleration = 0;
		if (TargetCheckpoint >= Checkpoints.Num())
		{
			TargetCheckpoint = 0;	
		}
	}
}

void AMovingSphere::SetAcceleration(float NewAcceleration)
{
	BaseAcceleration = NewAcceleration;
}

void AMovingSphere::SetCheckpoints(TArray<FVector> NewCheckpoints)
{
	Checkpoints = NewCheckpoints;
}

void AMovingSphere::AddCheckpoint(FVector point)
{
	Checkpoints.Add(point);
}