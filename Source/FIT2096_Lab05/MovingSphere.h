// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "MovingSphere.generated.h"

UCLASS()
class FIT2096_LAB05_API AMovingSphere : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovingSphere();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddCheckpoint(FVector);
	void SetCheckpoints(TArray<FVector>);
	void SetAcceleration(float);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* SphereVisual;

	int32 TargetCheckpoint;

	UPROPERTY(EditAnywhere)
		TArray<FVector> Checkpoints;

	UPROPERTY(EditAnywhere)
		float BaseAcceleration;
	float CurrentAcceleration;

};
