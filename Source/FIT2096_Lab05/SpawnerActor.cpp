// Fill out your copyright notice in the Description page of Project Settings.

#include "TimerManager.h"
#include "SpawnerActor.h"

// Sets default values
ASpawnerActor::ASpawnerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StartText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("StartNumber"));
	StartText->SetHorizontalAlignment(EHTA_Center);
	StartText->SetWorldSize(150.0f);
	RootComponent = StartText;

	SpawnInterval = 3.f;
	StartTime = 3;
	SpawnCountdown = 0;
	Acceleration = 2.f;
}

// Called when the game starts or when spawned
void ASpawnerActor::BeginPlay()
{
	Super::BeginPlay();
	
	UpdateTimerDisplay();
	GetWorldTimerManager().SetTimer(StartTimerHandle, this, &ASpawnerActor::AdvanceTimer, 1.0f, true);
}

// Called every frame
void ASpawnerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (StartSpawning)
	{
		SpawnCountdown -= DeltaTime;
		if (SpawnCountdown <= 0 && SpawnObject)
		{
			AMovingSphere* TempReference = GetWorld()->SpawnActor<AMovingSphere>(SpawnObject,
				this->GetActorLocation(), FRotator::ZeroRotator);
			TempReference->SetCheckpoints(Points);
			TempReference->SetAcceleration(Acceleration);
			SpawnCountdown = SpawnInterval;
		}
	}
}

void ASpawnerActor::UpdateTimerDisplay()
{
	StartText->SetText(FString::FromInt(FMath::Max(StartTime, 0)));
}

void ASpawnerActor::AdvanceTimer()
{
	--StartTime;
	UpdateTimerDisplay();
	if (StartTime < 1)
	{
		GetWorldTimerManager().ClearTimer(StartTimerHandle);
		CountdownHasFinished();
	}
}

void ASpawnerActor::CountdownHasFinished()
{
	StartText->SetText(TEXT("GO!"));
	StartSpawning = true;
}
